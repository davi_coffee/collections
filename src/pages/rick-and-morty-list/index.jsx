import { useHistory, useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import CharactersList from "../../components/characters-list";
import { Pagination } from "antd";
import axios from "axios";
import AnimationDiv from "../../components/animation-div";

const RickAndMortyList = () => {
  const params = useParams();
  const history = useHistory();

  const [results, setResults] = useState([]);
  const [current, setCurrent] = useState(1);

  const getDataApi = (url) => {
    axios
      .get(url)
      .then((data) => {
        setResults([...data.data.results]);
      })
      .catch(setResults([]));
  };

  useEffect(() => {
    const idList = params.id;
    setCurrent(Number(idList));
    getDataApi(`https://rickandmortyapi.com/api/character/?page=${idList}`);
  }, [params.id]);

  const handlePagination = (page) => {
    history.push(`/rick-and-morty/list/${page}`);
  };

  return (
    <>
      <AnimationDiv>
        <Pagination
          size={"small"}
          responsive={true}
          current={current}
          onChange={handlePagination}
          total={340}
          showSizeChanger={false}
          showLessItems={true}
        />
        <CharactersList persons={results} />
      </AnimationDiv>
    </>
  );
};
export default RickAndMortyList;

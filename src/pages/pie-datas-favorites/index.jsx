import React, { useEffect, useState } from "react";
import { Pie } from "react-chartjs-2";
import { Container, Title } from "./styles";
import AnimationDiv from "../../components/animation-div";

const PieDatas = () => {
  const [dataPie, setDataSets] = useState({ labels: [], datasets: [] });

  useEffect(() => {
    const pokemonFavorite = JSON.parse(
      window.localStorage.getItem("pokemon_favorite")
    );
    const rickFavorite = JSON.parse(
      window.localStorage.getItem("rick_favorite")
    );
    const labels = ["Pokemon Favorites", "Rick And Morty Favorites"];
    const rValue = Math.floor(Math.random() * 255);
    const gValue = Math.floor(Math.random() * 255);
    const bValue = Math.floor(Math.random() * 255);
    let data = [];
    if (pokemonFavorite) {
      const scorePokemon = pokemonFavorite.length;
      data = [...data, scorePokemon];
    } else {
      data = [...data, 0];
    }
    if (rickFavorite) {
      const scoreRick = rickFavorite.length;
      data = [...data, scoreRick];
    } else {
      data = [...data, 0];
    }
    setDataSets({
      labels: labels,
      datasets: [
        {
          data: data,
          backgroundColor: [
            `rgba(${rValue},${gValue},${bValue},1)`,
            `rgba(${bValue},${rValue},${gValue},1)`,
            `rgba(${gValue},${bValue},${rValue},1)`,
          ],
          hoverBackgroundColor: [
            `rgba(${rValue},${gValue},${bValue},0.4)`,
            `rgba(${bValue},${rValue},${gValue},0.4)`,
            `rgba(${gValue},${bValue},${rValue},0.4)`,
          ],
        },
      ],
    });
  }, []);

  return (
    <AnimationDiv>
      <Container>
        <Title>Proportion of favorite characters</Title>
        <Pie data={dataPie} />
      </Container>
    </AnimationDiv>
  );
};

export default PieDatas;

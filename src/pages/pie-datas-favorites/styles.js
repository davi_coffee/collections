import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  margin: 0 auto;

  @media (min-width: 900px) {
    max-width: 900px;
  }
`;

export const Title = styled.h2`
  color: #444;
  font-size: 20px;

  @media (min-width: 768px) {
    font-size: 30px;
  }
`;

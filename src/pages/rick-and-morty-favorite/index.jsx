import { useEffect, useState } from "react";
import CharactersList from "../../components/characters-list";
import AnimationDiv from "../../components/animation-div";

const RickAndMortyFavorite = () => {
  const [favorites, setFavorites] = useState([]);
  const [deleteFavorite, setDelete] = useState(false);

  const handleResults = () => {
    setDelete(!deleteFavorite);
  };

  useEffect(() => {
    const results = window.localStorage.getItem("rick_favorite");
    if (results) {
      setFavorites(JSON.parse(results));
    } else {
      setFavorites([]);
    }
  }, [deleteFavorite]);

  return (
    <>
      <AnimationDiv>
        <CharactersList
          persons={favorites}
          description={true}
          handleResults={handleResults}
        />
      </AnimationDiv>
    </>
  );
};

export default RickAndMortyFavorite;

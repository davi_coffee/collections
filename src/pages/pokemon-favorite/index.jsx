import { useEffect, useState } from "react";
import CharactersList from "../../components/characters-list";
import AnimationDiv from "../../components/animation-div";

const PokemonFavorite = () => {
  const [favorites, setFavorites] = useState([]);
  const [deleteFavorite, setDelete] = useState(false);

  const handleResults = () => {
    setDelete(!deleteFavorite);
  };

  useEffect(() => {
    const results = JSON.parse(window.localStorage.getItem("pokemon_favorite"));
    if (results) {
      setFavorites(results);
    } else {
      setFavorites([]);
    }
  }, [deleteFavorite]);

  return (
    <>
      <AnimationDiv>
        <CharactersList
          persons={favorites}
          description={true}
          handleResults={handleResults}
        />
      </AnimationDiv>
    </>
  );
};

export default PokemonFavorite;

import { useEffect, useState } from "react";
import CharactersList from "../../components/characters-list";
import { Pagination } from "antd";
import { useHistory, useParams } from "react-router-dom";
import axios from "axios";
import AnimationDiv from "../../components/animation-div";

const PokemonList = () => {
  const params = useParams();
  const history = useHistory();

  const [results, setResults] = useState([]);
  const [current, setCurrent] = useState(1);

  const getDataApi = (url) => {
    axios.get(url).then((data) => {
      setResults([...data.data.results]);
    });
  };

  useEffect(() => {
    const idList = params.id;
    setCurrent(Number(idList));
    getDataApi(
      `https://pokeapi.co/api/v2/pokemon?offset=${(idList - 1) * 20}&limit=20`
    );
  }, [params.id]);

  const handlePagination = (page) => {
    history.push(`/pokemon/list/${page}`);
  };

  return (
    <>
      <AnimationDiv>
        <Pagination
          size={"small"}
          responsive={true}
          current={current}
          onChange={handlePagination}
          total={340}
          showSizeChanger={false}
          showLessItems={true}
        />
        <CharactersList persons={results} />
      </AnimationDiv>
    </>
  );
};

export default PokemonList;

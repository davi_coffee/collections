import React, { useEffect, useState } from "react";
import { Pie } from "react-chartjs-2";
import { Container, Title } from "./styles";
import axios from "axios";
import AnimationDiv from "../../components/animation-div";

const PieDatas = () => {
  const [dataPie, setDataSets] = useState({ labels: [], datasets: [] });

  const constructorSetData = (datas) => {
    const labels = datas.titles;
    const rValue = Math.floor(Math.random() * 255);
    const gValue = Math.floor(Math.random() * 255);
    const bValue = Math.floor(Math.random() * 255);

    setDataSets({
      labels: labels,
      datasets: [
        {
          data: datas.data,
          backgroundColor: [
            `rgba(${rValue},${gValue},${bValue},1)`,
            `rgba(${bValue},${rValue},${gValue},1)`,
            `rgba(${gValue},${bValue},${rValue},1)`,
          ],
          hoverBackgroundColor: [
            `rgba(${rValue},${gValue},${bValue},0.4)`,
            `rgba(${bValue},${rValue},${gValue},0.4)`,
            `rgba(${gValue},${bValue},${rValue},0.4)`,
          ],
        },
      ],
    });
  };

  useEffect(() => {
    let datas = { titles: [], data: [] };

    axios
      .get("https://rickandmortyapi.com/api/character/?page=1")
      .then((res) => {
        datas = {
          titles: [...datas.titles, "Rick and Morty"],
          data: [...datas.data, res.data.info.count],
        };
        if (datas.data.length === 2) {
          constructorSetData(datas);
        }
      });
    axios
      .get("https://pokeapi.co/api/v2/pokemon?offset=20&limit=20")
      .then((res) => {
        datas = {
          titles: [...datas.titles, "Pokemon"],
          data: [...datas.data, res.data.count],
        };

        if (datas.data.length === 2) {
          constructorSetData(datas);
        }
      });
  }, []);

  return (
    <AnimationDiv>
      <Container>
        <Title>Proportion of favorite characters</Title>
        <Pie data={dataPie} />
      </Container>
    </AnimationDiv>
  );
};

export default PieDatas;

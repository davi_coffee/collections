import { Route, Switch } from "react-router-dom";
import RickAndMortyList from "../pages/rick-and-morty-list";
import RickAndMortyFavorite from "../pages/rick-and-morty-favorite";
import PokemonList from "../pages/pokemon-list";
import PokemonFavorite from "../pages/pokemon-favorite";
import PieDatas from "../pages/pie-datas";
import PieDatasFavorites from "../pages/pie-datas-favorites";
import { AnimatePresence } from "framer-motion";

const Router = () => {
  return (
    <AnimatePresence>
      <Switch>
        <Route exact path="/">
          <PieDatas />
        </Route>

        <Route exact path="/favorites-charts">
          <PieDatasFavorites />
        </Route>

        <Route exact path="/rick-and-morty/list/:id">
          <RickAndMortyList />
        </Route>

        <Route exact path="/pokemon/list/:id">
          <PokemonList />
        </Route>

        <Route exact path="/rick-and-morty/favorites">
          <RickAndMortyFavorite />
        </Route>

        <Route exact path="/pokemon/favorites">
          <PokemonFavorite />
        </Route>
      </Switch>
    </AnimatePresence>
  );
};

export default Router;

import { List } from "antd";
import { Container } from "./styles";
import CharacterCard from "../character-card";

const CharactersList = ({ persons, description, handleResults }) => {
  const constructorId = (person) => {
    const brokenUrl = person.url.split("/");
    return brokenUrl[brokenUrl.length - 2];
  };

  return (
    <Container>
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 2,
          lg: 3,
          xl: 4,
          xxl: 6,
        }}
        dataSource={persons}
        renderItem={(person) => (
          <List.Item key={person.id ? person.id : constructorId(person)}>
            <CharacterCard
              person={person}
              description={description}
              handleResults={handleResults}
            />
          </List.Item>
        )}
      />
    </Container>
  );
};

export default CharactersList;

import { Box } from "./styles";

const ContainerMain = ({ children }) => {
  return <Box>{children}</Box>;
};

export default ContainerMain;

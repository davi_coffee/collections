import styled from "styled-components";

export const Box = styled.div`
  width: 90%;
  margin: 30px auto 50px auto;
`;

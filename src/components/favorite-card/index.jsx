import { useState, useEffect } from "react";
import { HeartOutlined, HeartFilled } from "@ant-design/icons";

const FavoriteCard = ({ person, choiceApi, handleResults }) => {
  const [isFavorite, setFavorite] = useState(false);

  const handleFavorite = (_evt) => {
    if (window.localStorage.getItem(choiceApi)) {
      let setFavorites = JSON.parse(window.localStorage.getItem(choiceApi));
      let isContains = setFavorites.find(({ url }) => url === person.url);
      if (isContains) {
        setFavorites = setFavorites.filter(({ url }) => {
          return url !== person.url;
        });
      } else {
        setFavorites = [...setFavorites, person];
      }
      window.localStorage.setItem(choiceApi, JSON.stringify(setFavorites));
    }
    if (!window.localStorage.getItem(choiceApi)) {
      window.localStorage.setItem(choiceApi, JSON.stringify([person]));
    }
    if (isFavorite) {
      handleResults();
    }
    setFavorite(!isFavorite);
  };
  useEffect(() => {
    if (window.localStorage.getItem(choiceApi)) {
      let setFavorites = JSON.parse(window.localStorage.getItem(choiceApi));
      let isContains = setFavorites.find(({ url }) => url === person.url);
      if (isContains) {
        setFavorite(true);
      }
    }
  }, []);

  return (
    <>
      {isFavorite ? (
        <HeartFilled onClick={handleFavorite} />
      ) : (
        <HeartOutlined onClick={handleFavorite} />
      )}
    </>
  );
};

export default FavoriteCard;

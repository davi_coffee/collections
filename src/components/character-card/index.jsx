import { Card, Avatar } from "antd";
import { useEffect, useState } from "react";
import FavoriteCard from "../favorite-card";
import axios from "axios";

const { Meta } = Card;

const CharacterCard = ({ person, description, handleResults }) => {
  const [descriptionPerson, setDescriptionPerson] = useState(null);

  const constructorSrc = () => {
    const brokenUrl = person.url.split("/");
    const id = brokenUrl[brokenUrl.length - 2];
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
  };

  useEffect(() => {
    if (description) {
      if (person.image) {
        const phrase = `${person.name} is a ${person.species} and is ${person.status} ,currently lives on ${person.location.name}`;
        setDescriptionPerson(phrase);
      } else {
        const brokenUrl = person.url.split("/");
        const id = brokenUrl[brokenUrl.length - 2];
        axios.get(`https://pokeapi.co/api/v2/pokemon/${id}/`).then((data) => {
          const phrase = `${data.data.name} has ${data.data.base_experience} experience and is ${data.data.height} tall. Has the ability ${data.data.abilities[0].ability.name}`;
          setDescriptionPerson(phrase);
        });
      }
    }
  }, []);

  return (
    <Card
      hoverable
      style={{ width: 300 }}
      cover={
        <img
          alt={person.name}
          src={person.image ? person.image : constructorSrc()}
        />
      }
      actions={[
        <FavoriteCard
          person={person}
          choiceApi={person.image ? "rick_favorite" : "pokemon_favorite"}
          handleResults={handleResults}
        />,
      ]}
    >
      <Meta
        avatar={<Avatar>{person.name[0]}</Avatar>}
        title={person.name}
        description={descriptionPerson}
      />
    </Card>
  );
};

export default CharacterCard;

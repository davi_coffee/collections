import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import {
  UnorderedListOutlined,
  HeartOutlined,
  PieChartOutlined,
} from "@ant-design/icons";

import { Menu } from "antd";
const { SubMenu } = Menu;

const NavMenu = () => {
  const history = useHistory();
  const { pathname } = useLocation();

  const handleClick = (evt) => {
    if (evt.key === "chart") {
      history.push("/");
    } else {
      history.push(evt.key);
    }
  };

  return (
    <Menu onClick={handleClick} selectedKeys={pathname} mode="horizontal">
      <SubMenu icon={<UnorderedListOutlined />} title="Lists">
        <Menu.Item key="/rick-and-morty/list/1">Rick and Morty</Menu.Item>
        <Menu.Item key="/pokemon/list/1">Pokemon</Menu.Item>
      </SubMenu>

      <SubMenu icon={<HeartOutlined />} title="Favorites">
        <Menu.Item key="/rick-and-morty/favorites">Rick and Morty</Menu.Item>
        <Menu.Item key="/pokemon/favorites">Pokemon</Menu.Item>
      </SubMenu>

      <SubMenu icon={<PieChartOutlined />} title="Favorites">
        <Menu.Item key="/favorites-charts">Pie Chart Favorites</Menu.Item>
        <Menu.Item key="chart">Pie chart</Menu.Item>
      </SubMenu>
    </Menu>
  );
};
export default NavMenu;

import "antd/dist/antd.css";
import "./App.css";
import Router from "./router";
import React from "react";
import NavMenu from "./components/nav-menu";
import ContainerMain from "./components/container-main";

function App() {
  return (
    <div className="App">
      <NavMenu />
      <ContainerMain>
        <Router />
      </ContainerMain>
    </div>
  );
}

export default App;
